using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using RecruitingGui.Base;

namespace RecruitingGui
{

  class RecruitingModel : ViewModelBase
  {

    public RecruitingModel()
    {
      Started = false;

      mcStartTimerCommand = new CommandBase(OnStartTimer, CanStartTimer);
      mcStopTimerCommand = new CommandBase(OnStopTimer, CanStopTimer);
      mcNewRecruitCommand = new CommandBase(OnNewRecruit, CanStopTimer);
      mcRemoveSelectedCommand = new CommandBase(OnRemoveSelected, CanRemoveSelected);

      recruitHandler = new RecruitHandler();

      Factions = new ObservableCollection<string>() {
        "Republic",
        "Empire"
      };
      SelectedFaction = Factions[0];

      Planets = new ObservableCollection<string>() {
        "Alderaan",
        "Balmorra",
        "Belsavis",
        "Corellia",
        "Coruscant",
        "CZ-198",
        "Darvannis",
        "Dromund Kaas",
        "Hoth",
        "Hutta",
        "Ilum",
        "Iokath",
        "Korriban",
        "Makeb",
        "Manaan",
        "Mek-Sha",
        "Nar Shaddaa",
        "Odessen",
        "Onderon",
        "Ord Mantell",
        "Oricon",
        "Ossus",
        "Quesh",
        "Rishi",
        "Taris",
        "Tatooine",
        "Tython",
        "Voss",
        "Yavin 4",
        "Zakuul",
        "Ziost"
      };
      SelectedPlanet = Planets[0];
    }

    public string PlayerName
    {
      get => msRecruitName;
      set => SetProperty(ref msRecruitName, value);
    }

    public string Output
    {
      get => msOutput;
      set => SetProperty(ref msOutput, value);
    }

    public bool Started { get; private set; }

    public ObservableCollection<Recruit> Recruits => recruitHandler.Recruits;

    public Recruit SelectedRecruit
    {
      get => mcSelectedRecruit;
      set => SetProperty(ref mcSelectedRecruit, value);
    }

    public ObservableCollection<string> Factions { get; }
    public string SelectedFaction { get; set; }
    public ObservableCollection<string> Planets { get; }
    public string SelectedPlanet { get; set; }

    public ICommand StartTimer => mcStartTimerCommand;
    public ICommand StopTimer => mcStopTimerCommand;
    public ICommand NewRecruit => mcNewRecruitCommand;
    public ICommand RemoveSelected => mcRemoveSelectedCommand;

    private string msRecruitName;
    private string msOutput;

    private DateTime mcStartTime;
    private DateTime mcStopTime;
    private Recruit mcSelectedRecruit;

    private readonly CommandBase mcStartTimerCommand;
    private readonly CommandBase mcStopTimerCommand;
    private readonly CommandBase mcNewRecruitCommand;
    private readonly CommandBase mcRemoveSelectedCommand;

    private readonly RecruitHandler recruitHandler;

    private void OnStartTimer(object commandParameter)
    {
      mcStartTime = DateTime.Now;
      Started = true;
      InvokeCanExecuteChanged();
    }

    private void OnStopTimer(object commandParameter)
    {
      mcStopTime = DateTime.Now;
      Started = false;
      if (Recruits.Count > 0)
      {
        var timeSpan = mcStopTime - mcStartTime;
        Output = $"SWTOR\n" +
                 $"Faction: {SelectedFaction}\n" +
                 $"Time spent: {timeSpan.Hours}hr(s) {timeSpan.Minutes}min(s) {timeSpan.Seconds}sec(s)\n" +
                 $"Total recruits: {Recruits.Count}";

        foreach (var recruit in Recruits)
        {
          Output += $"\n{recruit.PlayerName} on {recruit.PlanetName}";
        }
      }
      InvokeCanExecuteChanged();
    }

    private void OnNewRecruit(object commandParameter)
    {
      if (PlayerName != "")
      {
        recruitHandler.Add(new Recruit(PlayerName, SelectedPlanet, DateTime.Now));
        PlayerName = "";
      }
      InvokeCanExecuteChanged();
    }

    private void OnRemoveSelected(object commandParameter)
    {
      recruitHandler.Remove(SelectedRecruit);
      InvokeCanExecuteChanged();
    }

    private bool CanStartTimer(object commadParameter) => !Started;
    private bool CanStopTimer(object commadParameter) => Started;
    private bool CanRemoveSelected(object commandParameter) => recruitHandler.Recruits.Count > 0;

    private void InvokeCanExecuteChanged()
    {
      mcStartTimerCommand.InvokeCanExecuteChanged();
      mcStopTimerCommand.InvokeCanExecuteChanged();
      mcNewRecruitCommand.InvokeCanExecuteChanged();
      mcRemoveSelectedCommand.InvokeCanExecuteChanged();
    }
  }
}
