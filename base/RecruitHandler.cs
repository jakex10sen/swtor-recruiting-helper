using System;
using System.Collections.ObjectModel;

namespace RecruitingGui.Base
{
  public class Recruit
  {
    public Recruit(string playerName, string planetName, DateTime dateTime)
    {
      PlayerName = playerName;
      PlanetName = planetName;
      RecruitDate = dateTime;
    }

    public string PlayerName { get; set; }
    public string PlanetName { get; set; }
    public DateTime RecruitDate { get; set; }
  }

  public class RecruitHandler
  {
    public RecruitHandler()
    {
      Recruits = new ObservableCollection<Recruit>();
    }

    public ObservableCollection<Recruit> Recruits { get; private set; }

    public void Add(Recruit newRecruit)
    {
      Recruits.Add(newRecruit);
    }

    public void Remove(Recruit recruit)
    {
      Recruits.Remove(recruit);
    }
  }
}
